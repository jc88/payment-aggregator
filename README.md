# payment-aggregator



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## OpenAPI

- [ ] [OpenAPI with examples](./Openapi/openapi.json)


## Environment variables

Before work you should set all env variables
- `DATABASE_URL` -> "postgresql://root:secret@localhost:5434/simple_bank?sslmode=disable"
- `API_PORT`     -> "8080",

## Make commands

- `make createpg`     -> create a new docker container
- `make createdb`     -> create a new DB inside this container
- `make dropdb`       -> drop DB
- `make migrateup`    -> add all migrations
- `make migratedown`  -> drop all migrations
- `make autogenerate` -> autogenerate from openAPI file models and a server

## Important to update

- error handling
- authentication
- maybe add FSM (finish state machine)
- more validation for each domain
- add bank API (maybe with fake data)
- async work
