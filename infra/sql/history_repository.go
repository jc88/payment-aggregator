package sql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/jc88/payment-aggregator/domain"
)

// PaymentRepository is self-describe
type HistoryRepository struct {
	PoolConn *pgxpool.Pool
}

// NewHistoryRepository is constructor
func NewHistoryRepository(p *pgxpool.Pool) HistoryRepository {
	return HistoryRepository{PoolConn: p}
}

func (r *HistoryRepository) Create(ctx context.Context, tr *domain.History) error {
	sqlReq := `INSERT INTO history_payments (id, transaction_id, status, amount, currency)
	VALUES($1, $2, $3, $4, $5);`
	cmd, err := r.PoolConn.Exec(
		ctx,
		sqlReq,
		tr.ID(),
		tr.TransactionID,
		tr.Status,
		tr.Amount,
		tr.Currency,
	)
	if err != nil {
		return fmt.Errorf("insert history entity with err: %v", err)
	}

	ok := cmd.Insert()
	if !ok {
		return fmt.Errorf("insert history entity with err: %v", err)
	}

	return err
}
