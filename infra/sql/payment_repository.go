package sql

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/jc88/payment-aggregator/domain"
)

// .Query(context.Background(), "select * from public.get_person_details($1)", id)

// PaymentRepository is self-describe
type PaymentRepository struct {
	PoolConn *pgxpool.Pool
}

// NewPaymentRepository is constructor
func NewPaymentRepository(p *pgxpool.Pool) PaymentRepository {
	return PaymentRepository{PoolConn: p}
}

func (r *PaymentRepository) Get(ctx context.Context, id uuid.UUID) (*domain.Payment, error) {
	var p domain.Payment

	err := r.PoolConn.QueryRow(
		ctx,
		`SELECT id, full_name, card_number, status, amount, currency, available_amount FROM payment WHERE id=$1;`,
		id,
	).Scan(
		&p.ID,
		&p.CreditCard.CustomerData,
		&p.CreditCard.Number,
		&p.Status,
		&p.Amount,
		&p.Currency,
		&p.AvailableAmount,
	)
	if err != nil {
		return nil, fmt.Errorf("get transaction by id with err: %v", err)
	}

	return &p, nil
}

func (r *PaymentRepository) Authorization(ctx context.Context, auth domain.AuthPayment) error {
	sqlReq := `INSERT INTO payment (id, full_name, card_number, status, amount, available_amount, currency, description)
	VALUES($1, $2, $3, $4, $5, $6, $7, $8);`
	cmd, err := r.PoolConn.Exec(
		ctx,
		sqlReq,
		auth.ID(),
		auth.CreditCard.CustomerData,
		auth.CreditCard.Number,
		auth.Status,
		auth.Amount,
		auth.Amount,
		auth.Currency,
		auth.Description,
	)
	if err != nil {
		return fmt.Errorf("insert authorization transaction with err: %v", err)
	}

	ok := cmd.Insert()
	if !ok {
		return fmt.Errorf("insert transaction with err: %v", err)
	}

	return err
}

func (r *PaymentRepository) UnlockByID(ctx context.Context, id uuid.UUID) error {
	cmd, err := r.PoolConn.Exec(
		ctx,
		"UPDATE payment SET lock=false WHERE id=$1",
		id,
	)
	if err != nil {
		return err
	}

	aff := cmd.RowsAffected()
	if aff == 0 {
		return fmt.Errorf("nothing to update  with id: %s", id)
	}

	return nil
}

func (r *PaymentRepository) ChangeStatusByID(ctx context.Context, id uuid.UUID, status domain.Status) error {
	cmd, err := r.PoolConn.Exec(
		ctx,
		"UPDATE payment SET status=$1, lock=false WHERE id=$2",
		status,
		id,
	)
	if err != nil {
		return err
	}

	aff := cmd.RowsAffected()
	if aff == 0 {
		return fmt.Errorf("nothing to update  with id: %s", id)
	}

	return nil
}

// RefundFull returns full amount: creates a new entity in refund table + updates in payment table
func (r *PaymentRepository) RefundFull(ctx context.Context, pEnt *domain.Payment, rEnt *domain.Refund) error {
	tx, err := r.PoolConn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return fmt.Errorf("get and lock transaction with err: %v", err)
	}

	defer func() {
		if err != nil {
			tx.Rollback(context.TODO())
		}

		tx.Commit(context.TODO())
	}()

	cmd, err := r.PoolConn.Exec(
		ctx,
		"UPDATE payment SET status=$1, lock=false, available_amount=0 WHERE id=$2",
		domain.RefundStatus,
		pEnt.ID,
	)
	if err != nil {
		return err
	}

	aff := cmd.RowsAffected()
	if aff == 0 {
		return fmt.Errorf("nothing to update  with id: %s", pEnt.ID)
	}

	sqlReq := `INSERT INTO refunds (id, parent_id, status, amount, currency)	
	VALUES($1, $2, $3, $4, $5);`
	cmd2, err := r.PoolConn.Exec(
		ctx,
		sqlReq,
		rEnt.ID(),
		pEnt.ID,
		domain.RefundStatus,
		rEnt.Amount,
		rEnt.Currency,
	)
	if err != nil {
		return fmt.Errorf("insert history entity with err: %v", err)
	}

	ok := cmd2.Insert()
	if !ok {
		return fmt.Errorf("insert history entity with err: %v", err)
	}

	return nil
}

// RefundPartly returns partly action
func (r *PaymentRepository) RefundPartly(ctx context.Context, pEnt *domain.Payment, rEnt *domain.Refund) error {
	tx, err := r.PoolConn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return fmt.Errorf("partly refund create transaction with err: %v", err)
	}

	defer func() {
		if err != nil {
			tx.Rollback(ctx)
		}

		tx.Commit(ctx)
	}()

	rAmount := pEnt.RefundedAmount + rEnt.Amount
	availableAmount := pEnt.AvailableAmount - rEnt.Amount

	cmd1, err := r.PoolConn.Exec(
		ctx,
		"UPDATE payment SET refunded_amount=$1, lock=false, available_amount=$2 WHERE id=$3",
		rAmount,
		availableAmount,
		pEnt.ID,
	)
	if err != nil {
		return fmt.Errorf("locking transaction  with err: %v", err)
	}

	aff := cmd1.RowsAffected()
	if aff == 0 {
		return fmt.Errorf("nothing to update in transaction with id: %s", pEnt.ID)
	}

	sqlReq := `INSERT INTO refunds (id, parent_id, status, amount, currency)	
	VALUES($1, $2, $3, $4, $5);`
	cmd2, err := r.PoolConn.Exec(
		ctx,
		sqlReq,
		rEnt.ID(),
		pEnt.ID,
		domain.RefundStatus,
		rEnt.Amount,
		rEnt.Currency,
	)
	if err != nil {
		return fmt.Errorf("insert history entity with err: %v", err)
	}

	ok := cmd2.Insert()
	if !ok {
		return fmt.Errorf("insert history entity with err: %v", err)
	}

	return nil
}

func (r *PaymentRepository) GetTransactionAndLock(ctx context.Context, id uuid.UUID) (*domain.Payment, error) {
	tx, err := r.PoolConn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return nil, fmt.Errorf("get and lock transaction with err: %v", err)
	}

	defer func() {
		if err != nil {
			tx.Rollback(context.TODO())
		}

		tx.Commit(context.TODO())
	}()

	var p domain.Payment

	err = r.PoolConn.QueryRow(
		ctx,
		`SELECT id, full_name, card_number, status, amount, currency, available_amount
		FROM payment WHERE id=$1;`,
		id,
	).Scan(
		&p.ID,
		&p.CreditCard.CustomerData,
		&p.CreditCard.Number,
		&p.Status,
		&p.Amount,
		&p.Currency,
		&p.AvailableAmount,
	)
	if err != nil {
		return nil, fmt.Errorf("get transaction by id with err: %v", err)
	}

	if p.Lock {
		return nil, domain.ErrTransactionAlreadyLocked
	}

	cmd2, err := r.PoolConn.Exec(
		ctx,
		"UPDATE payment SET lock=true WHERE id=$1",
		id,
	)
	if err != nil {
		return nil, fmt.Errorf("locking transaction  with err: %v", err)
	}

	aff := cmd2.RowsAffected()
	if aff == 0 {
		return nil, fmt.Errorf("nothing to update in transaction with id: %s", id)
	}

	return &p, nil
}

// CaptureFull approves full amount: creates a new entity in capture table + updates in payment table
func (r *PaymentRepository) CaptureFull(ctx context.Context, pEnt *domain.Payment, rEnt *domain.Capture) error {
	tx, err := r.PoolConn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return fmt.Errorf("get and lock transaction with err: %v", err)
	}

	defer func() {
		if err != nil {
			tx.Rollback(context.TODO())
		}

		tx.Commit(context.TODO())
	}()

	cmd, err := r.PoolConn.Exec(
		ctx,
		"UPDATE payment SET status=$1, lock=false, available_amount=0 WHERE id=$2",
		domain.CaptureStatus,
		pEnt.ID,
	)
	if err != nil {
		return err
	}

	aff := cmd.RowsAffected()
	if aff == 0 {
		return fmt.Errorf("nothing to update  with id: %s", pEnt.ID)
	}

	sqlReq := `INSERT INTO captures (id, parent_id, status, amount, currency)	
	VALUES($1, $2, $3, $4, $5);`
	cmd2, err := r.PoolConn.Exec(
		ctx,
		sqlReq,
		rEnt.ID(),
		pEnt.ID,
		domain.CaptureStatus,
		rEnt.Amount,
		rEnt.Currency,
	)
	if err != nil {
		return fmt.Errorf("insert capture entity with err: %v", err)
	}

	ok := cmd2.Insert()
	if !ok {
		return fmt.Errorf("insert capture entity with err: %v", err)
	}

	return nil
}

// CapturePartly returns partly action
func (r *PaymentRepository) CapturePartly(ctx context.Context, pEnt *domain.Payment, rEnt *domain.Capture) error {
	tx, err := r.PoolConn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return fmt.Errorf("partly capture create transaction with err: %v", err)
	}

	defer func() {
		if err != nil {
			tx.Rollback(ctx)
		}

		tx.Commit(ctx)
	}()

	availableAmount := pEnt.AvailableAmount - rEnt.Amount

	cmd1, err := r.PoolConn.Exec(
		ctx,
		"UPDATE payment SET lock=false, available_amount=$1 WHERE id=$2",
		availableAmount,
		pEnt.ID,
	)
	if err != nil {
		return fmt.Errorf("locking transaction  with err: %v", err)
	}

	aff := cmd1.RowsAffected()
	if aff == 0 {
		return fmt.Errorf("nothing to update in transaction with id: %s", pEnt.ID)
	}

	sqlReq := `INSERT INTO captures (id, parent_id, status, amount, currency)	
	VALUES($1, $2, $3, $4, $5);`
	cmd2, err := r.PoolConn.Exec(
		ctx,
		sqlReq,
		rEnt.ID(),
		pEnt.ID,
		domain.CaptureStatus,
		rEnt.Amount,
		rEnt.Currency,
	)
	if err != nil {
		return fmt.Errorf("insert capture entity with err: %v", err)
	}

	ok := cmd2.Insert()
	if !ok {
		return fmt.Errorf("insert capture entity with err: %v", err)
	}

	return nil
}
