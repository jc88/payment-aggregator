package sql

import (
	"database/sql"
	"errors"
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

var defaultDBMigrationsPath = "../..//migrations"

func RunMigrations(dcUrl string) error {
	// "postgres://localhost:5432/database?sslmode=enable"
	db, err := sql.Open("postgres", dcUrl)
	if err != nil {
		return err
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return err
	}

	q1 := MigrationsSrc(defaultDBMigrationsPath)

	m, err := migrate.NewWithDatabaseInstance(
		q1,
		"postgres", driver,
	)
	if err != nil {
		return nil
	}

	err = m.Up()
	if err != nil {
		if !errors.Is(err, migrate.ErrNoChange) {
			return err
		}

		fmt.Println(err)
	}

	return nil
}

// MigrationsSrc returns an absolute path for the migrations if present, otherwise a local path as default value.
func MigrationsSrc(path string) string {
	if _, err := os.Stat("migrations"); err == nil {
		return "file://migrations"
	}
	return "file://" + path
}
