package controlers

import (
	"encoding/json"
	"net/http"

	"github.com/ggwhite/go-masker"

	"github.com/labstack/echo/v4"
	"gitlab.com/jc88/payment-aggregator/Oas3"
	"gitlab.com/jc88/payment-aggregator/usecases"
	"go.uber.org/zap"
)

// error codes
var (
	RefundErrorCode = "REFUND-ERROR"
)

type PaymentImpl struct {
	svc    usecases.PaymentCase
	logger *zap.Logger
}

func NewPaymentImpl(svc usecases.PaymentCase, l *zap.Logger) *PaymentImpl {
	return &PaymentImpl{
		svc:    svc,
		logger: l,
	}
}

func (p *PaymentImpl) AuthPayment(w http.ResponseWriter, r *http.Request) {
	var authReq Oas3.Authorization

	err := json.NewDecoder(r.Body).Decode(&authReq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	dto := usecases.AuthPaymentDTO{
		Amount:       authReq.Amount,
		CustomerName: authReq.CustomerName,
		CardNumber:   authReq.CardNumber,
		Currency:     authReq.Currency,
		CVV:          authReq.Cvv,
		MonthExpiry:  authReq.MonthExpiry,
		YearExpiry:   authReq.YearExpiry,
	}

	id, err := p.svc.AuthorizationPayment(r.Context(), dto)

	var resp Oas3.PaymentStatus

	resp.UniqueId = id.String()
	resp.Success = Oas3.PaymentStatusSuccessSuccess
	resp.Amount = authReq.Amount
	resp.Currency = authReq.Currency

	err = json.NewEncoder(w).Encode(&resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
}

func (p *PaymentImpl) Auth(c echo.Context) error {
	var authReq Oas3.Authorization

	if err := c.Bind(&authReq); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	p.logger.Info(
		"authorization request",
		zap.String("customer name", authReq.CustomerName),
		zap.String("card number", masker.CreditCard(authReq.CardNumber)),
		zap.Float32("amount", authReq.Amount),
		zap.String("currency", authReq.Currency),
	)

	dto := usecases.AuthPaymentDTO{
		Amount:       authReq.Amount,
		CustomerName: authReq.CustomerName,
		CardNumber:   authReq.CardNumber,
		Currency:     authReq.Currency,
		CVV:          authReq.Cvv,
		MonthExpiry:  authReq.MonthExpiry,
		YearExpiry:   authReq.YearExpiry,
	}

	id, err := p.svc.AuthorizationPayment(c.Request().Context(), dto)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	var resp Oas3.PaymentStatus

	resp.UniqueId = id.String()
	resp.Success = Oas3.PaymentStatusSuccessSuccess
	resp.Amount = authReq.Amount
	resp.Currency = authReq.Currency

	p.logger.Info(
		"authorization request was completed",
		zap.String("unique id", resp.UniqueId),
		zap.Float32("amount", resp.Amount),
		zap.String("currency", resp.Currency),
		zap.Any("status", resp.Success),
	)

	return c.JSON(http.StatusOK, &resp)
}

func (p *PaymentImpl) Capture(c echo.Context) error {
	var req Oas3.Confirmation

	if err := c.Bind(&req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	p.logger.Info(
		"capture request",
		zap.String("uuid transaction", req.UniqueId),
		zap.Float32("uuid transaction", req.Amount),
	)

	dto := usecases.CaptureDTO{
		ID:       req.UniqueId,
		Amount:   req.Amount,
		Currency: req.Currency,
	}

	payment, err := p.svc.CapturePayment(c.Request().Context(), dto)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	var resp Oas3.SecondStageResponse

	resp.Success = Oas3.SecondStageResponseSuccessSuccess
	resp.Amount = payment.Amount
	resp.Currency = string(payment.Currency)

	p.logger.Info(
		"capture request was completed",
		zap.String("unique id", req.UniqueId),
		zap.Any("status", resp.Success),
	)

	return c.JSON(http.StatusOK, &resp)
}

func (p *PaymentImpl) Refund(c echo.Context) error {
	var req Oas3.Refund

	if err := c.Bind(&req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	p.logger.Info(
		"refund request",
		zap.String("uuid transaction", req.UniqueId),
		zap.Float32("uuid transaction", req.Amount),
	)

	dto := usecases.RefundDTO{
		ID:     req.UniqueId,
		Amount: req.Amount,
	}

	rEnt, err := p.svc.RefundPayment(c.Request().Context(), dto)
	if err != nil {
		errResp := Oas3.ErrorResponse{
			Description: err.Error(),
			ErrorCode:   RefundErrorCode,
		}

		return echo.NewHTTPError(http.StatusInternalServerError, errResp)
	}

	var resp Oas3.SecondStageResponse

	resp.Success = Oas3.SecondStageResponseSuccessSuccess
	resp.Amount = rEnt.Amount
	resp.Currency = string(rEnt.Currency)

	p.logger.Info(
		"capture request was completed",
		zap.String("unique id", req.UniqueId),
		zap.Any("status", resp.Success),
	)

	return c.JSON(http.StatusOK, &resp)
}

func (p *PaymentImpl) Void(c echo.Context) error {
	var authReq Oas3.Void

	if err := c.Bind(&authReq); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	p.logger.Info(
		"authorization request",
		zap.String("uuid transaction", authReq.UniqueId),
	)

	dto := usecases.VoidPaymentDTO{
		ID: authReq.UniqueId,
	}

	payment, err := p.svc.VoidPayment(c.Request().Context(), dto)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	var resp Oas3.SecondStageResponse

	resp.Success = Oas3.SecondStageResponseSuccessSuccess
	resp.Amount = payment.Amount
	resp.Currency = string(payment.Currency)

	p.logger.Info(
		"authorization request was completed",
		zap.String("unique id", authReq.UniqueId),
		zap.Any("status", resp.Success),
	)

	return c.JSON(http.StatusOK, &resp)
}

func (p *PaymentImpl) PaymentsByID(c echo.Context, id string) error {
	p.logger.Info(
		"get auth payment request",
		zap.String("uuid transaction", id),
	)
	pEnt, err := p.svc.GetAuthPayment(c.Request().Context(), id)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	var resp Oas3.PaymentStatus
	resp.Amount = pEnt.Amount
	resp.UniqueId = pEnt.ID.String()
	resp.Currency = string(pEnt.Currency)
	resp.Success = Oas3.PaymentStatusSuccessSuccess

	p.logger.Info(
		"get request was completed",
		zap.String("unique id", id),
		zap.Any("status", resp.Success),
	)

	return c.JSON(http.StatusOK, &resp)
}
