package main

import (
	"context"
	"log"
	"net/http"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/labstack/echo/v4"
	"go.uber.org/zap"

	"gitlab.com/jc88/payment-aggregator/Oas3"
	"gitlab.com/jc88/payment-aggregator/config"
	"gitlab.com/jc88/payment-aggregator/infra/controlers"
	sqlx "gitlab.com/jc88/payment-aggregator/infra/sql"
	"gitlab.com/jc88/payment-aggregator/usecases"
)

func main() {
	ctx := context.Background()

	conf, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	logger, _ := zap.NewProduction()
	defer logger.Sync()
	sugar := logger.Sugar()

	sugar.Infof("connect to sqk DB: %s", conf.DatabaseURL)

	dbPool, err := pgxpool.Connect(ctx, conf.DatabaseURL)
	if err != nil {
		log.Fatal(err)
	}

	defer dbPool.Close()

	err = sqlx.RunMigrations(conf.DatabaseURL)
	if err != nil {
		log.Fatal(err)
	}

	pRepo := sqlx.NewPaymentRepository(dbPool)
	hRepo := sqlx.NewHistoryRepository(dbPool)

	authUC := usecases.NewPaymentCase(&pRepo, &hRepo)

	c := controlers.NewPaymentImpl(authUC, logger)

	e := echo.New()
	Oas3.RegisterHandlers(e, c)
	if err := e.Start(":8080"); err != http.ErrServerClosed {
		log.Fatal(err)
	}
}
