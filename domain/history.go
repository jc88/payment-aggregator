package domain

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

type History struct {
	id            uuid.UUID
	TransactionID uuid.UUID
	Status        Status
	Amount        float32
	Currency      Currency
	CreatedAt     *time.Time
	UpdatedAt     *time.Time
}

// ID returns id field
func (ap *History) ID() uuid.UUID {
	return ap.id
}

// NewHistory is constructor
func NewHistory(trID uuid.UUID, currency Currency, amount float32, status Status) (*History, error) {
	var h History
	h.id = uuid.New()

	h.TransactionID = trID
	h.Status = status

	cur := Currency(currency)
	h.Currency = cur

	h.Amount = amount

	err := h.validate()
	if err != nil {
		return nil, fmt.Errorf("history entity validation with err: %v", err)
	}

	return &h, nil
}

func (h *History) validate() error {
	err := h.Currency.Validate()
	if err != nil {
		return err
	}

	err = h.Status.Validate()
	if err != nil {
		return err
	}

	return nil
}
