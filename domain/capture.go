package domain

import (
	"time"

	"github.com/google/uuid"
)

type Capture struct {
	id            uuid.UUID
	TransactionID uuid.UUID
	Status        Status
	Amount        float32
	Currency      Currency
	CreatedAt     *time.Time
	UpdatedAt     *time.Time
}

// ID returns id field
func (ap *Capture) ID() uuid.UUID {
	return ap.id
}

// NewCapture is constructor
func NewCapture(trID uuid.UUID, currency Currency, amount float32, status Status) *Capture {
	var h Capture
	h.id = uuid.New()

	h.TransactionID = trID
	h.Status = status

	cur := Currency(currency)
	h.Currency = cur

	h.Amount = amount

	return &h
}

func (h *Capture) validate() error {
	err := h.Currency.Validate()
	if err != nil {
		return err
	}

	err = h.Status.Validate()
	if err != nil {
		return err
	}

	return nil
}
