package domain

import "errors"

// Status is self-described
type Status string

// Valid statuses
const (
	AuthStatus    Status = "AUTHORIZATION"
	CaptureStatus Status = "CAPTURED"
	VoidStatus    Status = "VOIDED"
	RefundStatus  Status = "REFUNDED"
	FailedStatus  Status = "FAILED"
)

// ErrInvalidCurrencyCode is self-describe
var ErrInvalidStatusCode = errors.New("invalid status of transaction")

// Validate returns error if the status is not defined
func (c Status) Validate() error {
	switch c {
	case AuthStatus, CaptureStatus, VoidStatus, RefundStatus, FailedStatus:
		return nil
	default:
		return ErrInvalidStatusCode
	}
}
