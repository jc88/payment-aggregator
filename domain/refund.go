package domain

import (
	"time"

	"github.com/google/uuid"
)

type Refund struct {
	id            uuid.UUID
	TransactionID uuid.UUID
	Status        Status
	Amount        float32
	Currency      Currency
	CreatedAt     *time.Time
	UpdatedAt     *time.Time
}

// ID returns id field
func (ap *Refund) ID() uuid.UUID {
	return ap.id
}

// NewRefund is constructor
func NewRefund(trID uuid.UUID, currency Currency, amount float32, status Status) *Refund {
	var h Refund
	h.id = uuid.New()

	h.TransactionID = trID
	h.Status = status

	cur := Currency(currency)
	h.Currency = cur

	h.Amount = amount

	return &h
}

func (h *Refund) validate() error {
	err := h.Currency.Validate()
	if err != nil {
		return err
	}

	err = h.Status.Validate()
	if err != nil {
		return err
	}

	return nil
}
