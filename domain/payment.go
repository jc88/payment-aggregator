package domain

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"github.com/ShiraazMoollatjie/goluhn"
	"github.com/google/uuid"
)

// domain errors
var (
	ErrCardDateExpiration       = errors.New("card date expiration error")
	ErrCardMonthValidation      = errors.New("invalid card month error")
	ErrCardYearValidation       = errors.New("invalid card year error")
	ErrVoidIncorrectStatus      = errors.New("wrong status to Void payment")
	ErrCaptureIncorrectStatus   = errors.New("wrong status to capture payment")
	ErrRefundIncorrectStatus    = errors.New("wrong status to Refund payment")
	ErrRefundIncorrectAmount    = errors.New("wrong amount to Refund payment")
	ErrCaptureIncorrectAmount   = errors.New("wrong amount to Capture payment")
	ErrTransactionAlreadyLocked = errors.New("transaction is already locked")
)

// Payment describes a payment model
type Payment struct {
	ID              uuid.UUID
	Status          Status
	CreditCard      CreditCard
	Amount          float32
	Currency        Currency
	Description     string
	Lock            bool
	AvailableAmount float32
	RefundedAmount  float32
	ErrorCode       string
	ErrorDesc       string
	CreatedAt       *time.Time
	UpdatedAt       *time.Time
}

func NewRefundPayment() {

}

// ValidateVoidConditions is pre-validation action before VOID action
func (p *Payment) ValidateVoidConditions() error {
	if p.Status != AuthStatus {
		return ErrVoidIncorrectStatus
	}

	return nil
}

// ValidateCaptureConditions is pre-validation action before CAPTURE action
func (p *Payment) ValidateCaptureConditions(amount float32) error {
	if p.Status != AuthStatus {
		return ErrCaptureIncorrectStatus
	}

	if p.Amount < amount {
		return ErrCaptureIncorrectAmount
	}

	if (p.Amount < amount) || p.AvailableAmount == 0 || (p.AvailableAmount-amount) < 0 {
		return ErrCaptureIncorrectAmount
	}

	return nil
}

// ValidateRefundConditions is pre-validation action before REFUND action
func (p *Payment) ValidateRefundConditions(amount float32) error {
	if p.Status != AuthStatus {
		return ErrRefundIncorrectStatus
	}

	if p.Amount < amount {
		return ErrRefundIncorrectAmount
	}

	if (p.Amount < amount) || p.AvailableAmount == 0 || (p.AvailableAmount-amount) < 0 {
		return ErrRefundIncorrectAmount
	}

	return nil
}

// AuthPayment describes a payment model for authorization
type AuthPayment struct {
	id          uuid.UUID
	CreditCard  CreditCard
	Amount      float32
	Currency    Currency
	Status      Status
	Description string
}

// ID returns id field
func (ap *AuthPayment) ID() uuid.UUID {
	return ap.id
}

func (ap *AuthPayment) validate() error {
	return nil
}

// NewAuthPayment is constructor
func NewAuthPayment(amount float32, currency, status string, cc CreditCard) (AuthPayment, error) {
	var ap AuthPayment

	cur := Currency(currency)
	err := cur.Validate()
	if err != nil {
		return ap, err
	}

	ap.id = uuid.New()
	ap.Currency = cur
	ap.Status = AuthStatus
	ap.CreditCard = cc
	ap.Amount = amount

	err = ap.validate()
	if err != nil {
		return AuthPayment{}, err
	}

	return ap, nil
}

// CreditCard describes a credit card data
type CreditCard struct {
	CustomerData string
	Number       string
	ExpireMonth  string
	ExpireYear   string
}

// NewCreditCard is constructor
func NewCreditCard(customer, number, year, month string) (CreditCard, error) {
	var cc CreditCard

	cc.CustomerData = customer
	cc.Number = number
	cc.ExpireYear = year
	cc.ExpireMonth = month

	err := cc.validate()
	if err != nil {
		return CreditCard{}, err
	}

	return cc, nil
}

func (cc CreditCard) validate() error {
	err := goluhn.Validate(cc.Number)
	if err != nil {
		return fmt.Errorf("validation customer card with err: %v", err)
	}

	reYear := regexp.MustCompile("^?([0-9]{2})$")

	if ok := reYear.MatchString(cc.ExpireYear); !ok {
		return ErrCardYearValidation
	}

	reMonth := regexp.MustCompile("^(0[1-9]|1[0-2])$")

	if ok := reMonth.MatchString(cc.ExpireMonth); !ok {
		return ErrCardMonthValidation
	}

	t := time.Now()
	nowYear := t.Year()
	nowMonth := int(t.Month())

	intVar, err := strconv.Atoi(cc.ExpireYear)
	if err != nil {
		return err
	}

	customerYear := 2000 + intVar

	if nowYear > customerYear {
		return ErrCardDateExpiration
	}

	if nowYear < customerYear {
		return nil
	}

	customerMonth, err := strconv.Atoi(cc.ExpireMonth)
	if err != nil {
		return err
	}

	if nowMonth > customerMonth {
		return ErrCardDateExpiration
	}

	return nil
}
