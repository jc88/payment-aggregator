package domain

import "errors"

// Currency is region-specific type
type Currency string

// valid currency
const (
	CurrencyUS Currency = "USD"
	CurrencyEU Currency = "EUR"
	CurrencyPL Currency = "PLN"
)

// ErrInvalidCurrencyCode is self-describe
var ErrInvalidCurrencyCode = errors.New("invalid currency code")

// Validate returns error if the currency is not defined
func (c Currency) Validate() error {
	switch c {
	case CurrencyUS, CurrencyEU, CurrencyPL:
		return nil
	default:
		return ErrInvalidCurrencyCode
	}
}
