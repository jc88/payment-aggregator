BUILD_TIME=`date +%FT%T%z`
GIT_REVISION=`git rev-parse --short HEAD`
GIT_BRANCH=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
LDFLAGS=-ldflags "-s -X main.buildTime=${BUILD_TIME} -X main.gitRevision=${GIT_REVISION} -X main.gitBranch=${GIT_BRANCH}"

build:
	cd cmd/payaggr && go build .

dockerbuild:
	docker build --build-arg GITHUB_TOKEN=glpat-qxTtzqVbXS1EmRobtXaX -t payaggr:latest .

createpg:
	docker run --name postgres12 -p 5434:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=secret -d postgres:13-alpine

createdb:
	docker exec -it postgres12 createdb --username=root --owner=root simple_bank

dropdb:
	docker exec -it postgres12 dropdb simple_bank

migrateup:
	migrate -path migrations -database "postgresql://root:secret@localhost:5434/simple_bank?sslmode=disable" -verbose up

migratedown:
	migrate -path migrations -database "postgresql://root:secret@localhost:5434/simple_bank?sslmode=disable" -verbose down

generatemodels:
	oapi-codegen -generate chi-server openapi/openapi.json  > openapi/payments.gen.go

autogenerate:
	oapi-codegen oas3/oas3.json  > oas3/payments.gen.go

.PHONY: build dockerbuild createpg createdb dropdb migrateup migratedown generatemodels
