package usecases

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/jc88/payment-aggregator/domain"
)

// PaymentCase describes a process of authorization
type PaymentCase struct {
	pRepo PaymentRepo
	hRepo HistoryRepo
}

// NewPaymentCase is constructor
func NewPaymentCase(pr PaymentRepo, hr HistoryRepo) PaymentCase {
	return PaymentCase{pRepo: pr, hRepo: hr}
}

// AuthPaymentDTO is self-described
type AuthPaymentDTO struct {
	Amount       float32
	CardNumber   string
	Currency     string
	CustomerName string
	CVV          string
	MonthExpiry  string
	YearExpiry   string
}

// AuthorizationPayment executes authorization action of payment
func (us *PaymentCase) AuthorizationPayment(ctx context.Context, dto AuthPaymentDTO) (uuid.UUID, error) {
	cc, err := domain.NewCreditCard(dto.CustomerName, dto.CardNumber, dto.YearExpiry, dto.MonthExpiry)
	if err != nil {
		return uuid.UUID{}, err
	}

	authEnt, err := domain.NewAuthPayment(dto.Amount, dto.Currency, string(domain.AuthStatus), cc)
	if err != nil {
		return uuid.UUID{}, err
	}

	err = us.pRepo.Authorization(ctx, authEnt)
	if err != nil {
		return uuid.UUID{}, err
	}

	hEnt, err := domain.NewHistory(authEnt.ID(), authEnt.Currency, authEnt.Amount, authEnt.Status)
	if err != nil {
		return uuid.UUID{}, err
	}

	_ = us.hRepo.Create(ctx, hEnt)

	return authEnt.ID(), nil
}
