package usecases

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/jc88/payment-aggregator/domain"
)

// UnlockPayment executes unlock action of payment
func (us *PaymentCase) UnlockPayment(ctx context.Context, id uuid.UUID) error {
	return us.pRepo.UnlockByID(ctx, id)
}

// UnlockPayment executes unlock action of payment
func (us *PaymentCase) GetAuthPayment(ctx context.Context, idStr string) (*domain.Payment, error) {
	id, err := uuid.Parse(idStr)
	if err != nil {
		return nil, fmt.Errorf("parsing id with er: %v", err)
	}

	return us.pRepo.Get(ctx, id)
}
