package usecases

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/jc88/payment-aggregator/domain"
)

// RefundDTO is self-described
type RefundDTO struct {
	ID     string
	Amount float32
}

// RefundPayment executes capture action of payment
func (us *PaymentCase) RefundPayment(ctx context.Context, dto RefundDTO) (*domain.Refund, error) {
	id, err := uuid.Parse(dto.ID)
	if err != nil {
		return nil, fmt.Errorf("parsing DTO id with er: %v", err)
	}

	p, err := us.pRepo.GetTransactionAndLock(ctx, id)
	if err != nil {
		return nil, err
	}

	defer us.pRepo.UnlockByID(ctx, id)

	rEnt, err := us.ExecuteRefundPayment(ctx, dto, p)
	if err != nil {
		return nil, err
	}

	hEnt, err := domain.NewHistory(rEnt.ID(), rEnt.Currency, rEnt.Amount, domain.RefundStatus)
	if err != nil {
		return nil, err
	}

	_ = us.hRepo.Create(ctx, hEnt)

	return rEnt, nil
}

func (us *PaymentCase) ExecuteRefundPayment(ctx context.Context, dto RefundDTO, pEnt *domain.Payment) (*domain.Refund, error) {
	if err := pEnt.ValidateRefundConditions(dto.Amount); err != nil {
		return nil, fmt.Errorf("validation condition for REFUND action with err: %v", err)
	}

	rEnt := domain.NewRefund(pEnt.ID, pEnt.Currency, dto.Amount, domain.RefundStatus)

	if dto.Amount == pEnt.AvailableAmount {
		err := us.pRepo.RefundFull(ctx, pEnt, rEnt)
		if err != nil {
			return nil, fmt.Errorf("update transaction to REFUND status with err: %v", err)
		}

		return rEnt, nil
	}

	err := us.pRepo.RefundPartly(ctx, pEnt, rEnt)
	if err != nil {
		return nil, fmt.Errorf("partially refund with er: %v", err)
	}

	return rEnt, nil
}
