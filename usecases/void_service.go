package usecases

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/jc88/payment-aggregator/domain"
)

// VoidPaymentDTO is self-described
type VoidPaymentDTO struct {
	ID string
}

// VoidPayment executes void action of payment
func (us *PaymentCase) VoidPayment(ctx context.Context, dto VoidPaymentDTO) (*domain.Payment, error) {
	id, err := uuid.Parse(dto.ID)
	if err != nil {
		return nil, fmt.Errorf("parsing DTO id with er: %v", err)
	}

	p, err := us.pRepo.GetTransactionAndLock(ctx, id)
	if err != nil {
		return nil, err
	}

	defer us.pRepo.UnlockByID(ctx, id)

	if err = p.ValidateVoidConditions(); err != nil {
		return nil, fmt.Errorf("validation condition for VOID action with er: %v", err)
	}

	err = us.pRepo.ChangeStatusByID(ctx, id, domain.VoidStatus)
	if err != nil {
		return nil, fmt.Errorf("update transaction to VOID status with err: %v", err)
	}

	hEnt, err := domain.NewHistory(p.ID, p.Currency, p.Amount, domain.VoidStatus)
	if err != nil {
		return nil, err
	}

	_ = us.hRepo.Create(ctx, hEnt)

	return p, nil
}
