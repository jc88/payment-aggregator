package usecases

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/jc88/payment-aggregator/domain"
)

// CaptureDTO is self-described
type CaptureDTO struct {
	ID       string
	Amount   float32
	Currency string
}

// CapturePayment executes capture action of payment
func (us *PaymentCase) CapturePayment(ctx context.Context, dto CaptureDTO) (*domain.Capture, error) {
	id, err := uuid.Parse(dto.ID)
	if err != nil {
		return nil, fmt.Errorf("parsing DTO id with er: %v", err)
	}

	p, err := us.pRepo.GetTransactionAndLock(ctx, id)
	if err != nil {
		return nil, err
	}

	defer us.pRepo.UnlockByID(ctx, id)

	cEnt, err := us.ExecuteCapturePayment(ctx, dto, p)
	if err != nil {
		return nil, err
	}

	hEnt, err := domain.NewHistory(cEnt.ID(), cEnt.Currency, cEnt.Amount, domain.CaptureStatus)
	if err != nil {
		return nil, err
	}

	_ = us.hRepo.Create(ctx, hEnt)

	return cEnt, nil
}

func (us *PaymentCase) ExecuteCapturePayment(ctx context.Context, dto CaptureDTO, pEnt *domain.Payment) (*domain.Capture, error) {
	if err := pEnt.ValidateCaptureConditions(dto.Amount); err != nil {
		return nil, fmt.Errorf("validation condition for CAPTURE action with err: %v", err)
	}

	cEnt := domain.NewCapture(pEnt.ID, pEnt.Currency, dto.Amount, domain.RefundStatus)

	if dto.Amount == pEnt.AvailableAmount {
		err := us.pRepo.CaptureFull(ctx, pEnt, cEnt)
		if err != nil {
			return nil, fmt.Errorf("update transaction to CAPTURE status with err: %v", err)
		}

		return cEnt, nil
	}

	err := us.pRepo.CapturePartly(ctx, pEnt, cEnt)
	if err != nil {
		return nil, fmt.Errorf("partially capture with er: %v", err)
	}

	return cEnt, nil
}
