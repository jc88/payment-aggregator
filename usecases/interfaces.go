package usecases

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/jc88/payment-aggregator/domain"
)

type PaymentRepo interface {
	Get(ctx context.Context, id uuid.UUID) (*domain.Payment, error)
	UnlockByID(ctx context.Context, id uuid.UUID) error
	Authorization(ctx context.Context, auth domain.AuthPayment) error
	ChangeStatusByID(ctx context.Context, id uuid.UUID, status domain.Status) error
	RefundFull(ctx context.Context, p *domain.Payment, r *domain.Refund) error
	GetTransactionAndLock(ctx context.Context, id uuid.UUID) (*domain.Payment, error)
	RefundPartly(ctx context.Context, pEnt *domain.Payment, rEnt *domain.Refund) error
	CaptureFull(ctx context.Context, pEnt *domain.Payment, rEnt *domain.Capture) error
	CapturePartly(ctx context.Context, pEnt *domain.Payment, rEnt *domain.Capture) error
}

type HistoryRepo interface {
	Create(ctx context.Context, tr *domain.History) error
}
