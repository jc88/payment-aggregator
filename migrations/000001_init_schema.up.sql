CREATE TABLE IF NOT EXISTS payment
(
    id UUID PRIMARY KEY,
    full_name text NOT NULL,
    card_number character varying(20) NOT NULL,
    status text NOT NULL,
    amount NUMERIC NOT NULL,
    currency character varying(3) NOT NULL,
    lock bool DEFAULT false,
    available_amount NUMERIC,
    refunded_amount NUMERIC,
    error_code  character varying(10),
    error_desc  text,
    description text,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS history_payments
(
    id UUID PRIMARY KEY,
    transaction_id UUID NOT NULL,
    status text NOT NULL,
    amount NUMERIC NOT NULL,
    currency character varying(3) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS refunds
(
    id UUID PRIMARY KEY,
    parent_id UUID NOT NULL,
    status text NOT NULL,
    amount NUMERIC NOT NULL,
    currency character varying(3) NOT NULL,
    error_code  character varying(10),
    error_desc  text,
    description text,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS captures
(
    id UUID PRIMARY KEY,
    parent_id UUID NOT NULL,
    status text NOT NULL,
    amount NUMERIC NOT NULL,
    currency character varying(3) NOT NULL,
    error_code  character varying(10),
    error_desc  text,
    description text,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
