package config

import (
	"fmt"
	"os"
)

// Config struct
type Config struct {
	DatabaseURL string
	APIPort     string
}

// New function returns new struct
func New() (*Config, error) {
	c := Config{
		DatabaseURL: os.Getenv("DATABASE_URL"),
		APIPort:     os.Getenv("API_PORT"),
	}

	err := c.validate()
	if err != nil {
		return nil, fmt.Errorf("config validation with error: %v", err)
	}

	return &c, nil
}

func (c *Config) validate() error {
	return nil
}
