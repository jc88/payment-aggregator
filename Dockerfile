FROM golang:1.17.8-alpine3.14 AS builder
WORKDIR /root/
COPY go.mod .
RUN go mod download

COPY . .
RUN apk add --update --no-cache make git

ARG GITHUB_TOKEN

ENV GOPRIVATE=gitlab.com/jc88/*
RUN git config --global url."https://${GITHUB_TOKEN}@gitlab.com".insteadOf "https://gitlab.com"

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o payaggr cmd/payaggr/main.go

FROM alpine:3.15.0
WORKDIR /root/

RUN apk add --update --no-cache curl

COPY --from=builder /root/cmd/payaggr/payaggr .
COPY --from=builder /root/migrations migrations
EXPOSE 8086
CMD [ "./payaggr/payaggr" ]
